import { Component, OnInit } from '@angular/core';
import { CardDeck } from '../lib/CardDeck';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent extends CardDeck implements OnInit {
  ngOnInit(): void {
    super.getDeck();
    super.getCards(2);
  }

  giveAnotherCard() {
    super.getCard();
  }

  resetGame() {
    super.getDeck();
  }
}

