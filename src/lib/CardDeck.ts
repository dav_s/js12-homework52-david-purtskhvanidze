const RANKS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const SUITS = ['diams', 'hearts', 'clubs', 'spades'];

export class Card {
  rank: string;
  suit: string;

  constructor(
    theRank: string,
    theSuit: string
  ) {
    this.rank = theRank;
    this.suit = theSuit;

    type Card = {
      rank: string,
      suit: string;
    }
  }

  getScore() {

  }
}

export class CardDeck {
  deck: object[] = [{}];
  cardsInGame: object[] = [];

  getDeck() {
    this.deck = [];
    for (let s = 0; s < SUITS.length; s++) {
      for (let r = 0; r < RANKS.length; r++) {
        this.deck.push(new Card(RANKS[r], SUITS[s]));
      }
    }
  }

  getCard() {
    return this.getCards(1);
  }

  getCards(howMany: number) {
    let step;
    for (step = 0; step < howMany; step++) {
      let randomNumber = Math.floor(Math.random() * (this.deck.length - 1 + 1)) + 1;
      let randomCard = this.deck.splice(randomNumber,1);



      this.cardsInGame.push(randomCard);


    }
    console.log(this.cardsInGame[0]);
  }
}
